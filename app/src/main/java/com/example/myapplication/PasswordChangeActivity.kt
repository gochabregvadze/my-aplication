package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class PasswordChangeActivity : AppCompatActivity() {
    private lateinit var editTextOldPassword: EditText
    private lateinit var editTextNewPassword: EditText
    private lateinit var editTextNewPassword2: EditText
    private lateinit var buttonChange: Button
    private lateinit var editTextPasseord: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_change)

        init()

        registerListeners()


    }

    private fun init() {
        editTextOldPassword = findViewById(R.id.editTextOldPassword)
        editTextNewPassword = findViewById(R.id.editTextNewPassword)
        editTextNewPassword2 = findViewById(R.id.editTextNewPassword2)
        buttonChange = findViewById(R.id.buttonChange)

    }

    private fun registerListeners() {
        buttonChange.setOnClickListener {

            val newPassword = editTextNewPassword.text.toString()
            val newPassword2 = editTextNewPassword2.text.toString()
            val password = editTextPasseord.text.toString()
            

            if (newPassword.isEmpty()) {
                Toast.makeText(this, "შეიყვანეთ ახალი პაროლი", Toast.LENGTH_SHORT).show()
            }
            else if (newPassword.length < 8){
                Toast.makeText(this, "პაროლი უნდა შეიცავდეს 8 სიმბოლოს", Toast.LENGTH_SHORT).show()
            }
            else if (newPassword !== newPassword2) {
                Toast.makeText(this, "გაიმეორეთ პაროლი", Toast.LENGTH_SHORT).show()
            }

            FirebaseAuth.getInstance()
                .currentUser?.updatePassword(newPassword)
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        Toast.makeText(this, "პაროლი წარმატებით შეიცვალა", Toast.LENGTH_SHORT).show()
                    }
                    else if (newPassword !== password)
                        Toast.makeText(this, "ძველი პაროლი არასწორია", Toast.LENGTH_SHORT).show()
                    else {
                        Toast.makeText(this, "დაფიქსირდა შეცდომა", Toast.LENGTH_SHORT).show()
                    }
                }
            

        }

    }

}