package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPassword2: EditText
    private lateinit var checkAgreeBox: CheckBox
    private lateinit var buttonRegister: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        init()
        registerListener()

    }
    private fun init() {

        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPassword2 = findViewById(R.id.editTextPassword2)
        checkAgreeBox = findViewById(R.id.checkAgreeBox)
        buttonRegister = findViewById(R.id.buttonRegister)

    }
    private fun registerListener() {
        buttonRegister.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val password2 = editTextPassword2.text.toString()

            if (email.isEmpty()) {
                Toast.makeText(this, "შეიყვანეთ E-mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else if (password.isEmpty()) {
                Toast.makeText(this, "შეიყვანეთ პაროლი", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else if (password2.isEmpty()) {
                Toast.makeText(this, "გაიმეორეთ პაროლი", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else if (password !== password2){
                Toast.makeText(this, "ჩაწერეთ იგივე პაროლი", Toast.LENGTH_SHORT).show()
            }

            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        val intent = Intent(this, LoginActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    else {
                        Toast.makeText(this, "დაფიქსირდა შეცდომა", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
}