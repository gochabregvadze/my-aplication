package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonLogin: Button
    private lateinit var buttonRegister: Button
    private lateinit var buttonPasswordReset: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if  (FirebaseAuth.getInstance().currentUser != null){
            gotoprofile()
        }

        setContentView(R.layout.activity_login)

        init()

        registerListeners()

    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonLogin = findViewById(R.id.buttonLogin)
        buttonRegister = findViewById(R.id.buttonRegister)
        buttonPasswordReset = findViewById(R.id.buttonPasswordReset)

    }

    private fun registerListeners() {

        buttonRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
        buttonPasswordReset.setOnClickListener {
            val intent = Intent(this, PasswordResetActivity::class.java)
            startActivity(intent)
        }

        buttonLogin.setOnClickListener {

            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()

            if (email.isEmpty()) {
                Toast.makeText(this, "შეიყვანეთ E-mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else if (password.isEmpty()){
                Toast.makeText(this, "შეიყვანეთ პაროლი", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else if (password.length < 8){
                Toast.makeText(this, "შეყვანილი პაროლი უნდა აღემატებოდეს 8 სიმბოლოს", Toast.LENGTH_LONG).show()
            }
            else {
            }
            if (email.contains("@")){
            }
            else{
                Toast.makeText(this, "E-mail შეიყვანეთ სწორად", Toast.LENGTH_SHORT).show()
            }


            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        gotoprofile()
                    }
                    else {
                        Toast.makeText(this, "პაროლი ან E-mail არასწორია", Toast.LENGTH_SHORT).show()
                    }
                }



        }


    }
    private fun gotoprofile() {
        val intent = Intent(this, ProfileActivity::class.java)
        startActivity(intent)
        finish()
    }

}